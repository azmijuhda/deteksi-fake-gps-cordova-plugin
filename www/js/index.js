/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
var fakegps_list = [];
var is_mock = true;

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');

    //get package fake gps
    getPackageFakeGPS();

}

function checkMockLocation(){
    SpinnerDialog.show(null,"Memeriksa keamanan ..");
    window.plugins.mocklocationchecker.check(
        function successCallback(result) {
            console.log("mock : " + JSON.stringify(result)); // true - enabled, false - disabled
            if(result[0].info == 'mock-true'){
                alert('Anda menggunakan lokasi palsu!!');
            }
          },
           
        function errorCallback(error) {
            console.log(error);
        }
    );
    SpinnerDialog.hide();
    return false;
}

function checkAppFakeGps(){
    SpinnerDialog.show(null,"Memeriksa keamanan ..");
    for(let list of this.fakegps_list){
        if(this.checkApp(list.package_name)){
            alert('Ada aplikasi fakegps di HP anda!!');
            break;
        }
    }
    SpinnerDialog.hide();
}

function checkApp(package){
    appAvailability.check(
        package.toString(), // Package Name
        function() {           // Success callback
            console.log('fakegps is available');
            // document.getElementById("cek").innerHTML = "Ada aplikasi fakegps di HP anda";
            alert('Ada aplikasi fakegps di HP anda!!');
            return true;
        },
        function() {           // Error callback
            console.log('fakegps is not available');
            return false;
            // document.getElementById("cek").innerHTML = "tidak ada WA";
        }
    );    
}

function getPackageFakeGPS(){
    SpinnerDialog.show(null,"Please wait ..");
    cordova.plugin.http.get('https://trialservice.geogiven.tech/ws-trial/master/app_fakegps', {}, {}, 
    function(response) {
        console.log('statussss : ' + JSON.stringify(response));
        if(response.status == 200){
            let result = JSON.parse(response.data);
            let item = result.item
            if (item.status == 'success'){
                this.fakegps_list = item.master;
            }else{
                console.error("No data!");    
            }
        }else{
            console.error("No network!");
        }
    }, function(response) {
        console.error('error : ' + response.error);
    });
    SpinnerDialog.hide();
}

function getPosition() {
    SpinnerDialog.show(null,"Please wait ..");
        var options = {
            enableHighAccuracy: true,
            maximumAge: 2000,
            timeout: 10000
        }
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
     
        function onSuccess(position) {
    
            // var element = document.getElementById('coords');
            document.getElementById("coords").innerHTML = 'Latitude: '  + position.coords.latitude      + '<br />' +
                                                           'Longitude: ' + position.coords.longitude     + '<br />' +
                                                           '<hr />';
            
            this.getAddress(position.coords.latitude, position.coords.longitude);
        };
     
        function onError(error) {
           alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
        }
    SpinnerDialog.hide();
 }

 function getAddress(lat,lng){
    nativegeocoder.reverseGeocode(success, failure, lat, lng, { useLocale: true, maxResults: 1 });
 
    function success(result) {
      var firstResult = result[0];
      console.log("First Result: " + JSON.stringify(firstResult));

      document.getElementById("address").innerHTML = firstResult.thoroughfare + ' ' +
                                                    firstResult.subThoroughfare + ', ' +
                                                    firstResult.subLocality + ', ' +
                                                    firstResult.locality + ', ' +
                                                    firstResult.subAdministrativeArea + ', ' +
                                                    firstResult.administrativeArea + ' - ' +
                                                    firstResult.postalCode;
    }
     
    function failure(err) {
      console.log(err);
    }
 }