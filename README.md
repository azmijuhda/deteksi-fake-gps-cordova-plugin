Pada source ini mengunakan 2 metode mendeteksi fake gps
1. Melihat dari aplikasi yang diinstall user
2. Membaca lokasi yang bersifat mock/fake/palsu

Langkah Pertama :
 - npm install

Jika belum ada platform tambahkan dengan :
 - cordova platform add android

Untuk menjalankan aplikasi android :
 - cordova run android

Dalam source ini menggunakan plugin :
 - cordova-plugin-appavailability untuk membaca aplikasi apa saja yang diinstall user guna untuk mendeteksi bahwa aplikasi berjenis illegal dapat terbaca
 - cordova-plugin-mock-location-checker untuk membaca lokasi bersifat mock/fake/palsu


